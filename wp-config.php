<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define( 'DB_NAME', 'pahcbages' );

/** Tu nombre de usuario de MySQL */
define( 'DB_USER', 'root' );

/** Tu contraseña de MySQL */
define( 'DB_PASSWORD', '' );

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define( 'DB_HOST', 'localhost' );

/** Codificación de caracteres para la base de datos. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'l6w<w@1QR}8OwC.s.qSGhG~X2G-g-UY8M`Ms7@S()|(6ujoKI7569w{1qTJP{6cf' );
define( 'SECURE_AUTH_KEY', 'p`+^<dzuHk`<9;B:/9Y%S]E`iy3c!#2X06B7Sv]E(R3-ZF1/RwtmkV~B+!lhdXk5' );
define( 'LOGGED_IN_KEY', 'g5MR2jy4p3uUXPc%l?@Z,l&rJ$Jxnq;f<o=_1]w(UEC@#7(39B$<W(kMQQ|Q>t$j' );
define( 'NONCE_KEY', 'I(_uEOIG)?SQQQ8qP6LZXTyhN7/M&.k(W+M,,:v70w,zZWuNvoc>:1,X/9kuUsZu' );
define( 'AUTH_SALT', 'P<S]Zs<I*lIg-TJt|6}(9=bNRFD?Z?,qR}RE4LoTRC]}h+./(dy/=mo&xA|xnEOK' );
define( 'SECURE_AUTH_SALT', '53gh-`#x}9E)Lp#ut}pdb%LP-)-YYNjD(?KPgkQ#VK{[kV&=5r +yIB>ym1v~Ybs' );
define( 'LOGGED_IN_SALT', 'IT Y4AsZ`O_XxYn;v5i-Ttpm!PV2*F)& Q<WH7LTOi/@n ZIVFOxxi%2} O7]XyD' );
define( 'NONCE_SALT', 'S)W.;U6U<x!BAwGTzWQ4oYfwAD>w/B3JCPDBcuv/. ]:9`I(^{@W%6=A#D;q^GEp' );

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

